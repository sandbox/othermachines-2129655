
Migrate from Domain Configuration, Domain Theme (6.x, 7.x) to Domain Variable (7.x).

Originally posted by user @othermachines in migration thread:
https://drupal.org/node/1593548

This module defines variables in the 'domain' realm provided by Domain Variable
module and migrates the data from the {domain_conf} and {domain_theme} tables
to {variable_store}. 

** UPDATE: This module will also migrate from 7.x - 7.x.

** UPDATE: In order to support Domain Settings module, this module now migrates 
ALL variables in domain_conf table, not just those defined in the module
code. Even though some variables may not be recognized by Variable module, it
is easier to find them in variable_store than to slog through serialized data
in domain_conf. (Thanks, @joachim!)

See: support Domain Settings 
     https://www.drupal.org/node/2574599  

See: Differences to domain configuration/settings/theme
     https://drupal.org/node/1856898


STEPS:

1. Back up your database!

2. If upgrading from 6.x:

  * Upgrade Drupal core to 7.x (see core file UPGRADE.txt)

  * Go through steps of upgrading Domain Access module but DO NOT enable
    any submodules until this process is finished.

    See: Upgrading to Drupal 7 using Domain Access
    https://drupal.org/node/1074092

4. Enable this module and all dependencies.

5. Go to Administration > Structure > Migrate Domain Configuration

6. Click "Migrate".

7. Check your data in {variables_store} table, or via the UI for each domain:
   admin/structure/domain/view/[domain id]/variables
   (May need to enable Variable Admin module)

8. Uninstall modules Domain Configuration and Domain Theme.

9. If necessary, manually remove the following tables:
   {domain_conf}
   {domain_theme}

10.Disable, uninstall and remove this module.

