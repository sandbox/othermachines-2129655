<?php

/**
 * @file
 */

/**
 * Submission form that triggers the migration.
 */
function migrate_domain_conf_form($form, &$form_state) {
  $form['description'] = array(
    '#markup' => '<p>' . t('Click the button below to migrate data from Drupal 6.x modules <em>Domain Configuration</em> and <em>Domain Theme</em> to <em>Domain Variable 7.x</em>. This process may take a few minutes.') . '</p>',
  );
  $form['actions']['migrate'] = array(
    '#type' => 'submit',
    '#value' => t('Migrate'),
  );
  return $form;
}

/**
 * Submit handler for migrate_domain_conf_form().
 */
function migrate_domain_conf_form_submit(&$form, &$form_state) {

  // These are variables supported by Domain Variable.
  $supported = array(
    'site_name',
    'site_slogan',
    'site_frontpage',
    'site_mail',
    'anonymous',
    'theme_settings',
    'theme_[theme]_settings',
    'date_default_timezone',
    'cache',
    'cache_lifetime',
    'maintenance_mode',
    'maintenance_mode_message',
    'language_default',
    'menu_main_links_source',
    'menu_secondary_links_source',
  );

  // These are variables required by Domain Variable. Defaults will be found
  // if the data isn't available.
  $required = array(
    'site_name',
    'site_mail',
    'site_frontpage',
    'anonymous',
  );

  // Update the variables for realm 'domain'.
  variable_set('variable_realm_list_domain', $supported);

  // Load themes and theme settings defaults.
  module_load_include('inc', 'variable', 'includes/system.variable');
  $loaded_themes = system_variable_option_theme('', array());
  $theme_defaults = system_variable_theme_defaults('', array());

  $batch = array(
    'operations' => array(
      array('migrate_domain_conf_batch', array($loaded_themes, $theme_defaults, $supported, $required)),
      ),
    'title' => t('Migrating data'),
    'finished' => 'migrate_domain_conf_batch_finished',
    'init_message' => t('Migration is starting...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Sorry, we have encountered an error.'),
    'file' => drupal_get_path('module', 'migrate_domain_conf') . '/migrate_domain_conf.admin.inc',
  );
  batch_set($batch);
  batch_process('admin/structure/migrate_domain_conf');
}

/**
 * Batch processing function.
 */
function migrate_domain_conf_batch($loaded_themes, $theme_defaults, $supported, $required, &$context) {

  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_domain'] = 0;
    $context['sandbox']['max'] = db_query('SELECT COUNT(DISTINCT domain_id) FROM {domain} WHERE domain_id <> 1')->fetchField();
  }

  $limit = 5;

  // Get all domains (excluding primary domain).
  $result = migrate_domain_conf_get_domains($context['sandbox']['current_domain'], $limit);

  foreach ($result as $row) {

    $domain = domain_lookup($row->domain_id);

    // Migrate data form 'domain_conf' table to 'variable_store'.
    if (db_table_exists('domain_conf')) {

      // Create an array to keep track of processed variables.
      $processed = array();

      foreach (migrate_domain_conf_conf($domain['domain_id']) as $name => $v) {
        if (in_array($name, $supported)) {

          // This variable requires an object.
          if ($name == 'language_default') {
            if ($v == 'domain-conf-ignore') {
              continue;
            }
            else {
              $language_list = language_list();
              if (isset($language_list[$v])) {
                $v = $language_list[$v];
              }
            }
          }
          variable_store_set('domain', $domain['machine_name'], $name, $v);
          $processed[] = $name;
        }
        else {
          variable_store_set('domain', $domain['machine_name'], $name, $v);
        }
      }

      // Populate some required fields if no data was available.
      foreach ($required as $name) {
        if (!in_array($name, $processed)) {
          // We handle each in a different way.
          switch ($name) {
            case 'site_name':
              $v = $domain['sitename'];
              break;
            case 'site_mail':
              $v = variable_get('site_mail', ini_get('sendmail_from'));
              break;
            case 'site_frontpage':
              $v = variable_get('site_frontpage', 'node');
              break;
            case 'anonymous':
              $v = variable_get('anonymous', t('Anonymous'));
          }
          variable_store_set('domain', $domain['machine_name'], $name, $v);
        }
      }
    }

    // Migrate data from 'domain_theme' table to 'variable_store'.
    if (db_table_exists('domain_theme')) {
      $theme_settings = migrate_domain_conf_theme_settings($domain['domain_id']);
      if (!empty($theme_settings)) {
        foreach ($theme_settings as $theme) {
          if (isset($loaded_themes[$theme['theme']])) {

            // Create a new settings array based on available defaults.
            $new_settings = array();
            foreach ($theme_defaults as $k => $default_value) {
              $new_settings[$k] = isset($theme['settings'][$k])
                ? $theme['settings'][$k]
                : $default_value;
            }
            // Add an entry to {variable_store}.
            variable_store_set('domain', $domain['machine_name'], 'theme_' . $theme['theme'] . '_settings', $new_settings);
          }
        }
      }
    }
    $context['results'][] = check_plain($domain['sitename']);
    $context['sandbox']['progress']++;
    $context['sandbox']['current_domain'] = $domain['domain_id'];
    $context['message'] = t('Now processing %domain', array('%domain' => $domain['sitename']));
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Helper function. Retrieve all non-top-level domains.
 */
function migrate_domain_conf_get_domains($current_domain, $limit) {
  return db_select('domain')
    ->fields('domain', array('domain_id'))
    ->condition('domain_id', $current_domain, '>')
    ->orderBy('domain_id')
    ->range(0, $limit)
    ->execute();
}

/**
 * Helper function. Retrieve data from {domain_conf} table.
 */
function migrate_domain_conf_conf($domain_id) {
  $record = db_query('SELECT * FROM {domain_conf} WHERE domain_id = ' . $domain_id . ' LIMIT 1')->fetchObject();
  return $record ? unserialize($record->settings) : array();
}

/**
 * Helper function. Retrieve data from {domain_theme} table.
 */
function migrate_domain_conf_theme_settings($domain_id) {
  $result = db_query('SELECT * FROM {domain_theme} WHERE domain_id = ' . $domain_id);
  $records = array();
  foreach ($result as $record) {
    $records[] = array(
      'theme' => $record->theme,
      'settings' => unserialize($record->settings),
    );
  }
  return $records;
}

/**
 * Batch 'finished' callback.
 */
function migrate_domain_conf_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('%results domains processed.', array('%results' => count($results))));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE))), 'error');
  }

}

